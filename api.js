const axios = require('axios')

const api = axios.create({
  baseURL: 'http://localhost:5000/'
})

exports.saveTransaction = (params) => {
  return api.post(`/pump-transaction/`, {params})
}

exports.getTime = (id) => {
  console.log(id)
  return api.get(`/pump-transaction/${id}`)
}