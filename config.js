const path = require('path')
const { machineIdSync } = require('node-machine-id')

require('dotenv-safe').load({
  path: path.join(__dirname, '.env'),
  sample: path.join(__dirname, '.env.example')
})

module.exports = {
  DB_CONFIG: {
    CREDS: {
      server: process.env.SERVER,
      user: process.env.USER,
      password: process.env.PASSWORD,
      database: process.env.DB
    },
    POOL: {
      max: 10,
      min: 0,
      idleTimeoutMillis: 30000
    },
    OPTIONS: {
      encrypt: false,
      trustServerCertificate: false
    }
  },
  CLIENT_ID: machineIdSync(),
  INTERVAL_TIME: 5000
}