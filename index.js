const sql = require('mssql')
// const { format } = require('date-fns')
const config = require('./config')
const api = require('./api')

sql.connect(config.DB_CONFIG)

const start = async () => {

  //format(new Date(), 'yyyy-MM-dd HH:mm:ss')
  let lastTime = await api.getTime(config.CLIENT_ID)

  console.log(lastTime, 'LAST TIME FROM SERVER')

  setInterval(async () => {
    try {
      const newTransaction = await sql.query`SELECT * FROM TrdTrade WHERE PcRcvTime > ${lastTime}`
      if (newTransaction.recordset && newTransaction.recordset.length) {
        lastTime = newTransaction.recordset[newTransaction.recordset.length - 1].PcRcvTime
        await api.saveTransaction(JSON.stringify(newTransaction.recordset))
      }
    } catch (error) {
      console.log(`[pump-transaction-monitoring-service][error][${JSON.stringify(error)}]`)
    }
  }, config.INTERVAL_TIME)
}

start()